Contributed backends for a chatbot.

## Backend config for chatbot

```
    {
      "name": "name-of-backend",
      "kind": "chatbot",
      "triggers": ["^\\.desiredtrigger "],
      "addresses": ["123.45.67.89:1234"]
    },
```
