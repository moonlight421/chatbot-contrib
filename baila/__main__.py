#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
if sys.version_info < (3, 0):
  reload(sys)
  sys.setdefaultencoding('utf8')
else:
  raw_input = input

import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) # for proto

from proto import chatbot_pb2, chatbot_pb2_grpc
import grpc
from concurrent import futures
from grpc_reflection.v1alpha import reflection  # pip install --user grpcio-reflection

from optparse import OptionParser
import threading
import time

############

import baila

class Chatbot(object):

  def Respond(self,
              request,
              context,):
    # type: (chatbot_pb2.RespondRequest, grpc.ServicerContext) -> chatbot_pb2.RespondResponse
    """grpc method implementation. Assumes XMPP."""
    msg = request.chat_message.body
    frm = request.chat_message.source.address
    dest = request.chat_message.destination.address
    resp = baila.parseMsg(frm, msg)

    print >> sys.stderr, '<%s> %s => %s' % (frm, msg, resp)

    if not resp:
      raise Exception('sorry, not understood')

    if not request.chat_message.is_group_chat:
      new_dest = chatbot_pb2.ChatAddress(chat_protocol=chatbot_pb2.XMPP,
                                         address=frm)
    else:
      chan = '/'.join(frm.split('/')[:-1])
      new_dest = chatbot_pb2.ChatAddress(chat_protocol=chatbot_pb2.XMPP,
                                         address=chan)

    # TODO: set create_time
    # TODO: set in_reply_to etc
    resp = chatbot_pb2.RespondResponse(
        chat_message=chatbot_pb2.ChatMessage(
            body=resp['body'],
            source=request.chat_message.destination,
            destination=new_dest,
            is_group_chat=request.chat_message.is_group_chat))

    yield resp


############

def grpc_run(addr='', port=9654, block=False):
    if not block:
        grpc_run_thread(addr, port).start()
    else:
        grpc_run_imp(addr, port)

class grpc_run_thread(threading.Thread):
    def __init__(self, addr, port):
        threading.Thread.__init__(self)
        self.grpc_addr = addr
        self.grpc_port = port
    def run(self):
        grpc_run_imp(addr=self.grpc_addr,
                     port=self.grpc_port)

def grpc_run_imp(addr, port):
    chatbot = Chatbot()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    chatbot_pb2_grpc.add_ChatBotServicer_to_server(
        chatbot, server)
    SERVICE_NAMES = (
        chatbot_pb2.DESCRIPTOR.services_by_name['ChatBot'].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)
    if addr=='':
        server.add_insecure_port('[::]:%d' % port)
    else:
        server.add_insecure_port('%s:%d' % (addr, port))
    server.start()
    while True:
        _ONE_DAY_IN_SECONDS = 60 * 60 * 24
        time.sleep(_ONE_DAY_IN_SECONDS)

def main():
    # Setup the command line arguments.
    optp = OptionParser()

    optp.add_option("-g", "--grpc_addr", dest="grpc_addr",
                    help=("grpc port or address:port"))
    opts, args = optp.parse_args()

    grpc_port = None
    if opts.grpc_addr:
        grpc_addr = opts.grpc_addr
        grpc_addr = grpc_addr.split(':')
        if len(grpc_addr) > 2:
            print >> sys.stderr, 'bad grpc_addr'
            sys.exit(1)
        if len(grpc_addr) == 2:
            grpc_port = int(grpc_addr[1])
            grpc_addr = grpc_addr[0]
        else:
            grpc_port = int(grpc_addr[0])
            grpc_addr = ''

    if grpc_port:
        print >> sys.stderr, "running grpc server on %s:%d" % (grpc_addr, grpc_port)
        grpc_run(addr=grpc_addr, port=grpc_port, block=False)
    pass

if __name__ == '__main__':
    main()
